//
//  ShadowView
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

final class ShadowView: UIView {

    // MARK: - Properties
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    
    // MARK: - View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupView()
    }
    
    
    // MARK: - Setup
    
    private func setupView() {
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowColor = UIColor.black.cgColor
    }
}
