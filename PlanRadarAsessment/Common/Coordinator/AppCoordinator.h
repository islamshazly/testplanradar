//
//  AppCoordinator.h
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCoordinator.h"

@protocol Router;

@interface AppCoordinator : BaseCoordinator

#pragma mark - Properties

- (instancetype)initWithRouter:(id <Router>)router;

@end

