//
//  AppCoordinator.m
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import "AppCoordinator.h"
#import "PlanRadarAsessment-Swift.h"

@interface AppCoordinator()

@property (strong, nonatomic) id <Router> router;

@end

@implementation AppCoordinator

#pragma mark - Init

- (instancetype)initWithRouter:(id <Router>)router {
    if (self = [super init]) {
        self.router = router;
    }
    return self;
}

#pragma mark - Overriden

- (void)start {
    CityListCoordinator * coordinator = [[CityListCoordinator alloc] initWithRouter:self.router builder: [[CityListBuilderImplementation alloc] init]];
    [self coordinateTo:coordinator];
}

@end
