//
//  BaseCoordinator.h
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseCoordinator : NSObject

#pragma mark - Methods

- (void)coordinateTo:(BaseCoordinator *)coordinator;
- (void)free:(BaseCoordinator *)coordinator;
- (void)start;

@end

