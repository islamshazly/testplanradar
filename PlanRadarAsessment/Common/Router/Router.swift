//
//  Router.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

@objc protocol Router {
    func setRoot(presentable: UIViewController?, hideBar: Bool)
    func popToRoot(animated: Bool)
    func present(presentable: UIViewController?, animated: Bool)
    func dismiss(animated: Bool, completion: (() -> Void)?)
    func push(presentable: UIViewController?, animated: Bool, completion: (() -> Void)?)
    func pop(animated: Bool)
    typealias CompletionHandler = (() -> Void)
}
