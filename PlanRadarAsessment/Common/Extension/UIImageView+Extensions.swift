//
//  UIImageView+Extensions.swift
//  
//
//  Created by islam Elshazly on 16.07.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setRounded() {
        layer.cornerRadius = bounds.size.width / 2
        layer.masksToBounds = true
    }
}
