//
//  Formatter+Extensions.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import Foundation

extension Formatter {
    
    static var weatherFormatter: DateFormatter {
          let formatter = DateFormatter()
          let local = Locale(identifier: "en_US")
          formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          formatter.locale = Locale.current
          formatter.locale = local
      
          return formatter
      }
      
}
