//
//  CoreDataStack.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import Foundation
import CoreData
import RxSwift



class CoreDataStack {
    
    private let modelName: String
    
    init(modelName: String) {
        self.modelName = modelName
    }
    
    private lazy var storeContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores {
            (storeDescription, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)") }
        }
        return container
    }()
    
    lazy var managedContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    func saveContext () {
        guard managedContext.hasChanges else { return }
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Unresolved error \(error), \(error.userInfo)") }
    }
    
    func save(cityName name: String) -> Observable<City> {
        return Observable.create({ [unowned self] observable in
            
            // Create Entity
            let entity = NSEntityDescription.entity(forEntityName: "City", in: self.managedContext)
            
            // Initialize Record
            let record = City(entity: entity!, insertInto: self.managedContext)
            record.name = name
            self.managedContext.performAndWait {
                do {
                    try self.managedContext.save()
                    observable.onNext(record)
                } catch let error as NSError {
                    print("could not save, managedobject \(error), \(error.userInfo)")
                    return observable.onError(error)
                }
            }
            return Disposables.create {}
        })
    }
    
    func fetchCities() -> Observable<[City]> {
        let request: NSFetchRequest<City> = City.fetchRequest()
        request.returnsObjectsAsFaults = false
        do {
            let result = try managedContext.fetch(request)
            for data in result {
                print(data)
            }
            return Observable.just(result)
        } catch {
            print("fetch request failed, managedobject")
            return Observable.empty()
        }
    }
    
    func deleteCity(cityName name: String) -> Observable<Bool>{
        let request: NSFetchRequest<City> = City.fetchRequest()
        request.returnsObjectsAsFaults = false
        do {
            let cities = try managedContext.fetch(request)
            for city in cities {
                if city.name == name {
                    managedContext.delete(city)
                    break
                }
            }
            return Observable.just(true)
        } catch {
            print("fetch request failed, managedobject")
            return Observable.just(false)
        }
    }
    
}
