//
//  AlertView.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

@objc extension UIViewController {
    
    // MARK: - Show Alert
    
    func showAlert(error: Error) {
        self.showAlert(title: "OPS", string: error.localizedDescription)
    }
    
    func showAlert(title: String = "Info", string: String) {
        
        let alert = UIAlertController(title: title,
                                      message: string,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
}

