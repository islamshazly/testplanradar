//
//  LoadingView.swift
//
//  Created by islam Elshazly on 16.07.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import SVProgressHUD

// MARK: - LoadingViewable

protocol LoadingViewable {
    func startLoadingIndicator()
    func stopLoadingIndicator()
}

final class LoadingView: NSObject {
    @objc class func startLoadingIndicator() {
        //SVProgressHUD.show(withStatus: "asdasd")
    }
    
    @objc class func stopLoadingIndicator() {
        //SVProgressHUD.dismiss()
    }
}


// MARK: - LoadingViewable Default Implementation

extension LoadingViewable where Self: UIViewController {
    
    func startLoadingIndicator() {
        SVProgressHUD.show()
    }
    
    func stopLoadingIndicator() {
        SVProgressHUD.dismiss()
    }
}

// MARK: - UIViewController implements LoadingViewable

extension UIViewController: LoadingViewable {}
