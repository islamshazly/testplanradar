//
//  NetworkConstants.swift
//
//  Created by islam Elshazly on 10/07/2019.
//  Copyright © 2019 islam Elshazly. All rights reserved.
//

extension C {
    enum NetworkURL {
        static let weather = "https://api.openweathermap.org/data/2.5/weather"
        static let weatherIcon = "https://openweathermap.org/img/w/%@.png"
    }
    
    enum Network {
        static let header = ["Content-Type": "application/json"]
    }
}
