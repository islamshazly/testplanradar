//
//  Constants.swift
//
//  Created by islam Elshazly on 10/07/2019.
//  Copyright © 2019 islam Elshazly. All rights reserved.
//

import UIKit

enum C {
    static let appName = "PlanRadar"
    static let themeColor  = #colorLiteral(red: 0.1064742729, green: 0.3803782463, blue: 0.6719145775, alpha: 1)
}
