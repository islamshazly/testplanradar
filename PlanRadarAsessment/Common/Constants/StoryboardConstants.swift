//
//  StoryboardConstants.swift
//
//  Created by islam Elshazly on 10/07/2019.
//  Copyright © 2019 islam Elshazly. All rights reserved.
//

extension C {
    enum Storyboard: String {
        case weather
        
        var name: String {
            return self.rawValue.capitalized
        }
    }
}
