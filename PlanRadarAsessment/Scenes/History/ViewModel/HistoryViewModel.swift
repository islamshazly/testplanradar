//
//  WeatherListViewModel.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import Foundation
import RxSwift

// MARK: - Protocols

protocol HistoryViewModelInput {
    var backToCitiesList: AnyObserver<Void> { get }
    var selectWeatherInfo: AnyObserver<WeatherViewModel> { get }
}

protocol HistoryViewModelOutput {
    var title: Observable<String> { get }
    var historyList: Observable<[WeatherViewModel]> { get }
    var showCitiesList: Observable<Void> { get }
    var showWeatherInfo: Observable<WeatherViewModel> { get }
}

protocol HistoryViewModelType {
    var inputs: HistoryViewModelInput { get }
    var outputs: HistoryViewModelOutput { get }
}

final class HistoryViewModel: HistoryViewModelInput, HistoryViewModelOutput, HistoryViewModelType {
 
    // MARK: - Inputs & Outputs
    
    var inputs: HistoryViewModelInput { return self }
    var outputs: HistoryViewModelOutput { return self }
    
    // MARK: - Inputs
    
    var backToCitiesList: AnyObserver<Void>
    var selectWeatherInfo: AnyObserver<WeatherViewModel>
    
    // MARK: - Outputs
    
    var title: Observable<String>
    var historyList: Observable<[WeatherViewModel]>
    var showCitiesList: Observable<Void>
    var showWeatherInfo: Observable<WeatherViewModel>
    
    // MARK: - Init
    
    init(city: City) {
        
        title = BehaviorSubject<String>(value: city.name.uppercased() + "\n" + "HISTORICAL")
        
        let _backToCitiesList = PublishSubject<Void>()
        self.backToCitiesList = _backToCitiesList.asObserver()
        self.showCitiesList = _backToCitiesList.asObservable()
        
        let _selectWeatherInfo = PublishSubject<WeatherViewModel>()
        self.selectWeatherInfo = _selectWeatherInfo.asObserver()
        self.showWeatherInfo = _selectWeatherInfo.asObservable()
        
        if let weathers = city.weatherList {
            
            var arr = [WeatherViewModel]()
            
            for case let weather as WeatherInfo in weathers {
                let weatherViewModel = WeatherViewModel(weatherInfo: weather)
                arr.append(weatherViewModel)
            }
            historyList = BehaviorSubject(value: arr)
        } else {
            historyList = BehaviorSubject(value: [])
        }
        
    }
}
