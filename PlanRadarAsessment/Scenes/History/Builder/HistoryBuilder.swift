//
//  HistoryBuilder.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 28.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

// MARK: - HistoryBuilder

protocol HistoryBuilder {
    func build() -> HistoryViewController
}

// MARK: - HistoryBuilderImplementation

final class HistoryBuilderImplementation: HistoryBuilder {
    
    let city: City
    
    init(city: City) {
        self.city = city
    }
    
    // MARK: - Build
    
    func build() -> HistoryViewController {
        
        let historyViewController = HistoryViewController.initFromStoryboard(storyboard: .weather)
        let viewModel = HistoryViewModel(city: city)
        historyViewController.viewModel = viewModel
        
        return historyViewController
    }
}
