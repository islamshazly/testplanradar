//
//  WeatherListViewController.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift

final class HistoryViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet private var tableView: StateHandlerTableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    
    var viewModel: HistoryViewModelType!
    private let disposeBag = DisposeBag()
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initialSetup()
        setupUI()
    }
    
    // MARK: - Setup
    
    private func initialSetup() {
        
        DispatchQueue.main.async {
            self.setupBindings()
        }
    }
    
    
    func setupBindings() {
        
        let inputs = viewModel.inputs
        let outputs = viewModel.outputs
        
        tableView.rx.modelSelected(WeatherViewModel.self)
            .bind(to: inputs.selectWeatherInfo)
            .disposed(by: disposeBag)
        
        dismissButton.rx.tap
            .bind(to: inputs.backToCitiesList)
            .disposed(by: disposeBag)
        
        outputs.historyList.bind(to: tableView.rx.items(cellIdentifier: HistoryCell.identifier, cellType: HistoryCell.self)) {
            (_, weather, cell) in
            cell.configure(weather: weather)
        }.disposed(by: disposeBag)
        
        outputs.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    private func setupUI() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(cellClass: HistoryCell.self)
    }
    
}
