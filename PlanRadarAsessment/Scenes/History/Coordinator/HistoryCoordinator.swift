//
//  HistoryCoordinator.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 28.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift

protocol HistoryCoordinatorDelegate: class {
    
    func didSelectWeatherInfo(_ weatherInfo: WeatherViewModel)
}

class HistoryCoordinator: BaseCoordinator {

    // MARK: - Properties
    
    private let router: Router
    private let builder: HistoryBuilder
    private let disposeBag = DisposeBag()
    weak var delegate: HistoryCoordinatorDelegate?
    
    // MARK: - Init
    
    init(router: Router, builder: HistoryBuilder) {
        self.router = router
        self.builder = builder
    }
    
    // MARK: - Overriden
    
    override func start() {
        let historyViewController = builder.build()
        router.present(presentable: historyViewController, animated: true)
        setupNavigationBindings(viewModel: historyViewController.viewModel)
    }
    
    // MARK: - Setup
    
    private func setupNavigationBindings(viewModel: HistoryViewModelType) {
        viewModel.outputs.showCitiesList
            .subscribe(onNext: { [weak self] city in self?.router.dismiss(animated: true, completion: nil) })
            .disposed(by: disposeBag)
        
        viewModel.outputs.showWeatherInfo
        .subscribe(onNext: { [weak self] weather in self?.navigateToCityDetails(weather: weather) })
        .disposed(by: disposeBag)
    }
    
    // MARK: - Navigation
    
    func navigateToCityDetails(weather: WeatherViewModel) {
        self.router.dismiss(animated: true) {
            self.delegate?.didSelectWeatherInfo(weather)
        }
    }
    
}
