//
//  HistoryCellTableViewCell.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

final class HistoryCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(weather: WeatherViewModel) {
        timeLabel.text = weather.timeString
        descriptionLabel.text = weather.fullDescription
    }
    
}
