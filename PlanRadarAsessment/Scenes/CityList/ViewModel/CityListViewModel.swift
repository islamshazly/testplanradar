//
//  CityListViewModel.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: - Protocols

protocol CityListViewModelInputs {
    var addCity: AnyObserver<String> { get }
    var deleteCity: AnyObserver<IndexPath> { get }
    var selectHistory: AnyObserver<IndexPath> { get }
    var selectCity: AnyObserver<City> { get }
}

protocol CityListViewModelOutputs {
    var title: Observable<String> { get }
    var cities: BehaviorRelay<[City]> { get }
    var showHistory: Observable<City> { get }
    var showCityDetails: Observable<City> { get }
}

protocol CityListViewModelType {
    var inputs: CityListViewModelInputs { get }
    var outputs: CityListViewModelOutputs { get }
}

// MARK: - Class

final class CityListViewModel: CityListViewModelInputs, CityListViewModelOutputs, CityListViewModelType {
    
    // MARK: - Inputs & Outputs
    
    var inputs: CityListViewModelInputs { return self }
    var outputs: CityListViewModelOutputs { return self }
    
    // MARK: - Inputs
    
    let addCity: AnyObserver<String>
    let deleteCity: AnyObserver<IndexPath>
    var selectHistory: AnyObserver<IndexPath>
    let selectCity: AnyObserver<City>
    
    // MARK: - Outputs
    
    let title: Observable<String>
    let cities: BehaviorRelay<[City]>
    var showHistory: Observable<City>
    var showCityDetails: Observable<City>
    
    // MARK: - Private Properties
    
    private let disposeBag = DisposeBag()
    
    init(repository: CityRepository) {
        
        title = BehaviorSubject<String>(value: "CITIES")
        
        self.cities = BehaviorRelay<[City]>(value: [])
        
        let _selectHistory = PublishSubject<IndexPath>()
        self.selectHistory = _selectHistory.asObserver()
        self.showHistory = Observable.empty()
        
        let _selectCity = PublishSubject<City>()
        self.selectCity = _selectCity.asObserver()
        self.showCityDetails = _selectCity
        
        let _addCity = PublishSubject<String>()
        self.addCity = _addCity.asObserver()
        
        let _deleteCity = PublishSubject<IndexPath>()
        self.deleteCity = _deleteCity.asObserver()
        
        self.showHistory = _selectHistory.asObservable().map { [unowned self] in self.cities.value[$0.row] }
        
        repository.fetchCities()
            .subscribe(onNext: { [weak self] cityList in
                guard let self = self else { return }
                self.cities.accept(cityList)
            })
            .disposed(by: disposeBag)
        
        _addCity.flatMapLatest { str in
            return repository.add(cityName: str)
        }
        .subscribe(onNext: { [weak self] city in
            guard let self = self else { return }
            self.cities.accept(self.cities.value + [city])
        })
            .disposed(by: disposeBag)
        
        _deleteCity.subscribe(onNext: { [weak self] indexPath in
            guard let self = self else { return }
            repository.deleteCity(self.cities.value[indexPath.row])
            var arr = self.cities.value
            arr.remove(at: indexPath.row)
            self.cities.accept(arr)
        })
            .disposed(by: disposeBag)
        
    }
}
