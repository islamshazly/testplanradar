//
//  WeatherViewModel.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import Foundation

@objc
final class WeatherViewModel: NSObject {
    @objc var cityName: String = ""
    @objc var time: Date
    @objc var cityDescription: String = ""
    @objc var temp: Float = 0
    @objc var humidity: Int = 0
    @objc var windSpeed: Float = 0
    @objc var iconID: String = ""
    @objc var tempC: String {
        return "\(temp)C"
    }
    @objc var humidityPerc: String {
        return "\(humidity)%"
    }
    @objc var windSpeedKM: String {
        return "\(humidity)%"
    }
    @objc var fullDescription: String {
        return cityDescription + " , " + tempC
    }
    @objc var info: String {
        return "Weather information for " + cityName + " received on " + timeString
    }
    var timeString: String {
        return Formatter.weatherFormatter.string(from: time)
    }
    
    var iconLink: URL {
        return URL(string: "http://openweathermap.org/img/w/\(iconID).png")!
    }
    
    @objc init(weather: Weather) {
        cityName = weather.name
        temp = weather.weatherMain.temp
        humidity = weather.weatherMain.humidity
        windSpeed = weather.wind.speed
        cityDescription = weather.weathers.first!.desc
        iconID = weather.weathers.first!.iconID
        time = Date()
    }
    
    @objc init(weatherInfo: WeatherInfo) {
        temp = weatherInfo.temp
        humidity = Int(weatherInfo.humidity)
        windSpeed = weatherInfo.windSpeed
        cityDescription = weatherInfo.cityDescription
        iconID = weatherInfo.iconID ?? ""
        time = weatherInfo.timeStamp
    }

}
