//
//  ViewController.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift

final class CityListViewController: UIViewController {
    
    // MARK: - Properities
    
    @IBOutlet private var tableView: StateHandlerTableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    
    var viewModel: CityListViewModelType!
    private let disposeBag = DisposeBag()
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tableView.deselectSelectedRow()
    }
    
    // MARK: - Setup
    
    private func initialSetup() {
        
        DispatchQueue.main.async {
            self.setupBindings()
        }
    }
    
    func setupBindings() {
        
        let inputs = viewModel.inputs
        let outputs = viewModel.outputs
        
        tableView.rx.modelSelected(City.self)
            .bind(to: inputs.selectCity)
            .disposed(by: disposeBag)
        
        tableView.rx.itemAccessoryButtonTapped
            .bind(to: inputs.selectHistory)
            .disposed(by: disposeBag)
        
        tableView.rx.itemDeleted
            .bind(to: inputs.deleteCity)
            .disposed(by: disposeBag)
        
        outputs.cities.bind(to: tableView.rx.items(cellIdentifier: CityCell.identifier, cellType: CityCell.self)) {
            (_, city, cell) in
            cell.configuareCell(city: city)
        }.disposed(by: disposeBag)
        
        outputs.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    private func setupUI() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(cellClass: CityCell.self)
    }
    
    // MARK: - Actions
    
    @IBAction func AddCityAction(_ sender: Any) {
        self.showAlertTextField()
    }
    
    // MARK: - Helpers
    
    func showAlertTextField() {
        let alert = UIAlertController(title: "City Name", message: nil, preferredStyle: .alert)
        alert.addTextField { [unowned self] (textField) in
            textField.rx.controlEvent(.editingDidEnd)
                .bind { [unowned self]  in
                    self.viewModel.inputs.addCity.onNext(textField.text!)}
                .disposed(by: self.disposeBag)
        }
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

