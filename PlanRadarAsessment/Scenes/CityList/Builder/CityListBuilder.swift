//
//  CityListBuilder.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

// MARK: - CityListBuilder

@objc protocol CityListBuilder {
    func build() -> CityListViewController
}

// MARK: - CitytBuilderImplementation

final class CityListBuilderImplementation: NSObject, CityListBuilder {
    
    // MARK: - Build
    
    func build() -> CityListViewController {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let cityListViewController = CityListViewController.initFromStoryboard(storyboard: .weather)
        let coreDataStack = appDelegate.coreDataStack
        let repository = CityRepositoryImplementation(manager: coreDataStack)
        let viewModel = CityListViewModel(repository: repository)
        cityListViewController.viewModel = viewModel
        
        return cityListViewController
    }
}
