//
//  CityCell.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27/08/2020.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

final class CityCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var nameLable: UILabel!
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Configure
    
    func configuareCell(city: City) {
        nameLable.text = city.name.uppercased()
    }
}
