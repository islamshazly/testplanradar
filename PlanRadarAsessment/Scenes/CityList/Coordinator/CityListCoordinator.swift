//
//  CityListCoordinator.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift

class CityListCoordinator: BaseCoordinator {

    // MARK: - Properties
    
    private let router: Router
    private let builder: CityListBuilder
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    @objc init(router: Router, builder: CityListBuilder) {
        self.router = router
        self.builder = builder
    }
    
    // MARK: - Overriden
    
    override func start() {
        let cityListViewController = builder.build()
        router.setRoot(presentable: cityListViewController, hideBar: true)
        setupNavigationBindings(viewModel: cityListViewController.viewModel)
    }
    
    // MARK: - Setup
    
    private func setupNavigationBindings(viewModel: CityListViewModelType) {
        viewModel.outputs.showHistory
            .subscribe(onNext: { [weak self] city in self?.navigateToHistory(city: city) })
            .disposed(by: disposeBag)
        
        viewModel.outputs.showCityDetails
            .subscribe(onNext: { [weak self] city in self?.navigateToCityDetails(city: city) })
            .disposed(by: disposeBag)
    }
    
    // MARK: - Navigation
    
    private func navigateToHistory(city: City) {
        let coordinator = HistoryCoordinator(router: router, builder: HistoryBuilderImplementation(city: city))
        coordinator.delegate = self
        self.coordinate(to: coordinator)
    }
    
    private func navigateToCityDetails(city: City) {
        let coordinator = CityDetailsCoordinator(router: router, builder: CityDetailsBuilderImplementation(city: city))
        self.coordinate(to: coordinator)
    }
    
}

extension CityListCoordinator: HistoryCoordinatorDelegate {
    
    func didSelectWeatherInfo(_ weatherInfo: WeatherViewModel) {
        let coordinator = CityDetailsCoordinator(router: router, builder: CityDetailsBuilderImplementation(weatherViewModel: weatherInfo))
        self.coordinate(to: coordinator)
    }
    
}
