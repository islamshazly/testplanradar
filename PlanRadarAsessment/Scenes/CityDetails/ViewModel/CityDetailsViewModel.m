//
//  CityDetailsViewModel.m
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import "CityDetailsViewModel.h"
#import "PlanRadarAsessment-Swift.h"

@interface CityDetailsViewModel()

@property (strong, nonatomic) id <WeatherRepository> repository;
@property (strong, nonatomic) WeatherViewModel *weather;
@property (strong, nonatomic) City *city;

@end

@implementation CityDetailsViewModel

#pragma mark - Init

- (instancetype)initWithRepository:(id <WeatherRepository>)repository city:(City *)city weatherViewModel:(WeatherViewModel *)weather {
    if (self = [super init]) {
        _title = city.name;
        self.repository = repository;
        self.weather = weather;
        self.city = city;
    }
    return self;
}

- (void)requestWeather {
    
    if (_city != NULL) {
        __weak CityDetailsViewModel *weakSelf = self;
        
        self.loadingIndicatorBlock(true);
        
        [self.repository fetchWeatherWithCity:_city completionBlock:^(Weather * weather, NSError * error) {
           
            if (weather != NULL) {
                WeatherViewModel *model = [[WeatherViewModel alloc] initWithWeather: weather];
                weakSelf.returnBlock(model);
            } else {
                weakSelf.errorBlock(error);
            }
            weakSelf.loadingIndicatorBlock(false);
        }];
    } else if (_weather != NULL) {
        self.returnBlock(_weather);
    }
}

- (void)viewCanceled {
    self.backBlock();
}

@end
