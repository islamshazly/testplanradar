//
//  CityDetailsViewModel.h
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WeatherViewModel;
@class City;
@class WeatherViewModel;
@protocol WeatherRepository;

#pragma mark - Typedef

typedef void (^ReturnValueBlock) (WeatherViewModel *weather);
typedef void (^ErrorCodeBlock) (NSError *error);
typedef void (^BackBlock) (void);
typedef void (^LoadingIndicatorBlock) (Boolean shouldShow);

@interface CityDetailsViewModel : NSObject

#pragma mark - Properties

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, copy) ReturnValueBlock returnBlock;
@property (nonatomic, copy) ErrorCodeBlock errorBlock;
@property (nonatomic, copy) BackBlock backBlock;
@property (nonatomic, copy) LoadingIndicatorBlock loadingIndicatorBlock;

#pragma mark - Methods

- (instancetype)initWithRepository:(id <WeatherRepository>)repository city:(City *)city weatherViewModel:(WeatherViewModel *)weather;
- (void)requestWeather;
- (void)viewCanceled;

@end
