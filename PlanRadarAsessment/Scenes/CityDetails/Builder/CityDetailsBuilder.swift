//
//  CityDetailsBuilder.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 28.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

// MARK: - CityDetailsBuilder

@objc protocol CityDetailsBuilder {
    func build() -> CityDetailsViewController
}

// MARK: - CityDetailsBuilderImplementation

final class CityDetailsBuilderImplementation: NSObject, CityDetailsBuilder {
    
    var city: City?
    var weatherViewModel: WeatherViewModel?
    
    init(city: City? = nil, weatherViewModel: WeatherViewModel? = nil) {
        self.city = city
        self.weatherViewModel = weatherViewModel
    }
    
    // MARK: - Build
    
    func build() -> CityDetailsViewController {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let cityDetailsViewController = CityDetailsViewController.initFromStoryboard(storyboard: .weather)
        let coreDataStack = appDelegate.coreDataStack
        
        let repository = WeatherRepositoryImplementation(apiClient: RESTClientImplementation(),
                                                         manager: coreDataStack)
        let viewModel = CityDetailsViewModel(repository: repository, city: self.city, weatherViewModel: weatherViewModel)
        cityDetailsViewController.viewModel = viewModel
        
        return cityDetailsViewController
    }
}
