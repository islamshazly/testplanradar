//
//  CityDetailsCoordinator.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 28.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

class CityDetailsCoordinator: BaseCoordinator {
    
    // MARK: - Properties
    
    private let router: Router
    private let builder: CityDetailsBuilder
    
    // MARK: - Init
    
    @objc init(router: Router, builder: CityDetailsBuilder) {
        self.router = router
        self.builder = builder
    }
    
    // MARK: - Overriden
    
    override func start() {
        let cityDetailsViewController = builder.build()
        router.push(presentable: cityDetailsViewController, animated: true, completion: nil)
        setupNavigationBindings(viewModel: cityDetailsViewController.viewModel)
    }
    
    private func setupNavigationBindings(viewModel: CityDetailsViewModel) {
        viewModel.backBlock = { [weak self] in
            self?.router.pop(animated: true)
        }
    }
}
