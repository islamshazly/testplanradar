//
//  CityDetailsViewController.h
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CityDetailsViewModel;

@interface CityDetailsViewController : UIViewController

@property (strong, nonatomic) CityDetailsViewModel *viewModel;

@end
