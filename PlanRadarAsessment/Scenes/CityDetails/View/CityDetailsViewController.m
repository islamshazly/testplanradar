//
//  CityDetailsViewController.m
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import "CityDetailsViewController.h"
#import "CityDetailsViewModel.h"
#import "PlanRadarAsessment-Swift.h"

@interface CityDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherIcon;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescription;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *windSpeedLabel;

@end

@implementation CityDetailsViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBindings];
    [self.viewModel requestWeather];
}

#pragma mark - Setup

- (void)setupBindings {
    __weak CityDetailsViewController *weakSelf = self;
    
    [self.viewModel setReturnBlock:^(WeatherViewModel *weather) {
        // update ui
        [weakSelf.infoLabel setText:weather.info];
        [weakSelf.weatherDescription setText:weather.fullDescription];
        [weakSelf.tempLabel setText:weather.tempC];
        [weakSelf.humidtyLabel setText:weather.humidityPerc];
        [weakSelf.windSpeedLabel setText:weather.windSpeedKM];
        
    }];
    
    [self.viewModel setErrorBlock:^(NSError *error) {
        [weakSelf showAlertWithError:error];
    }];
    
    [self.viewModel setLoadingIndicatorBlock:^(Boolean shouldShow) {
        if (shouldShow) {
            [LoadingView startLoadingIndicator];
        } else {
            [LoadingView stopLoadingIndicator];
        }
    }];
}

#pragma mark - Action

- (IBAction)backAction:(id)sender {
    [self.viewModel viewCanceled];
}

@end
