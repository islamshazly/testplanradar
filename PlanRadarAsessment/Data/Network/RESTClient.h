//
//  AFHTTPSessionManager.h
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AFHTTPSessionManager;

typedef void(^DataTaskCompletionBlock)(id jsonObject, NSError *error);
typedef void(^ImageCompletionBlock)(UIImage *jsonObject, NSError *error);

@protocol RESTClient <NSObject>

- (void)GET:(NSString *)url parameters:(NSDictionary *)parameters completion:(DataTaskCompletionBlock)block;
- (void)downloadImage:(NSString *)url completion:(ImageCompletionBlock)block;
- (void)cancelAllTasks;

@end


@interface RESTClientImplementation : NSObject<RESTClient>

@end
