//
//  WeatherData.swift
//  PlanRadarAsessment
//
//  Created by Hazem Maher on 28.08.20.
//  Copyright © 2020 Hazem Ahmed. All rights reserved.
//

import Foundation

@objc
final class WeatherData: NSObject, Codable {
    var _id: Int
    var desc: String
    var icon: String
    
    var iconID: String {
        return icon + "\(_id)"
    }
    
    private enum CodingKeys: String, CodingKey {
        case _id = "id"
        case desc = "description"
        case icon = "icon"
    }
}

