//
//  Weather.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

@objc
final class Weather: NSObject, Codable {
    
    var name: String
    var weathers: [WeatherData]
    var wind: Wind
    var weatherMain: WeatherMain
    
    private enum CodingKeys: String, CodingKey {
        case weathers = "weather"
        case name = "name"
        case wind = "wind"
        case weatherMain = "main"
    }
}



