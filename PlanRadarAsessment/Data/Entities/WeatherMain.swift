//
//  Main.swift
//  PlanRadarAsessment
//
//  Created by Hazem Maher on 28.08.20.
//  Copyright © 2020 Hazem Ahmed. All rights reserved.
//

import Foundation

@objc
class WeatherMain: NSObject, Codable {
    var temp: Float
    var humidity: Int
}
