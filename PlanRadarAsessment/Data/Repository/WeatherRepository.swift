//
//  WeatherRepository.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit

// MARK: - WeatherRepository

@objc protocol WeatherRepository {
    typealias completionBlock = (Weather?, Error?) -> Void
    func fetchWeather(city: City, completionBlock: @escaping completionBlock)
}

// MARK: - MoviesRepositoryImplementation

final class WeatherRepositoryImplementation: WeatherRepository {
    
    // MARK: - Properties
    
    private var apiClient: RESTClient
    private var manager: CoreDataStack
    
    // MARK: - Init
    
    init(apiClient: RESTClient, manager: CoreDataStack) {
        self.apiClient = apiClient
        self.manager = manager
    }
    
    // MARK: - API
    
    func fetchWeather(city: City, completionBlock: @escaping completionBlock) {
        
        self.apiClient.get(C.NetworkURL.weather, parameters: ["q": city.name, "appid":"f5cb0b965ea1564c50c6f1b74534d823"]) { (object, error) in
            
            if let error = error {
                completionBlock(nil, error)
            } else if let object = object {
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
                    let weather = try JSONDecoder().decode(Weather.self, from: data)
                    
                    let weatherInfo = WeatherInfo(context: self.manager.managedContext)
                    
                    weatherInfo.timeStamp = Date()
                    weatherInfo.humidity = Int64(weather.weatherMain.humidity)
                    weatherInfo.temp = weather.weatherMain.temp
                    weatherInfo.cityDescription = weather.weathers.first!.desc
                    weatherInfo.iconID = weather.weathers.first!.iconID
                    
                    if let weathers = city.weatherList?.mutableCopy() as? NSMutableOrderedSet {
                        weathers.add(weatherInfo)
                        city.weatherList = weathers
                    }
                    self.manager.saveContext()
                    completionBlock(weather, nil)
                    
                    } catch let error {
                    completionBlock(nil, error)
                }
                
            }
            
        }
        
    }
}
