//
//  CityRepository.swift
//  PlanRadarAsessment
//
//  Created by islam Elshazly on 27.08.20.
//  Copyright © 2020 islam Elshazly. All rights reserved.
//

import UIKit
import RxSwift
import CoreData

// MARK: - CityRepository

protocol CityRepository {
    func add(cityName name: String) -> Observable<City>
    func fetchCities() -> Observable<[City]>
    func deleteCity(_ city: City)
}

// MARK: - CityRepositoryImplementation

final class CityRepositoryImplementation: CityRepository {
    
    // MARK: - Properties
    
    private var manager: CoreDataStack
    
    // MARK: - Init
    
    init(manager: CoreDataStack) {
        self.manager = manager
    }
    
    // MARK: - API
    
    func add(cityName name: String) -> Observable<City> {
        let city = City(context: self.manager.managedContext)
        city.name = name
        self.manager.saveContext()
        return Observable.just(city)
    }
    
    func fetchCities() -> Observable<[City]> {
        
        let request: NSFetchRequest<City> = City.fetchRequest()
        request.returnsObjectsAsFaults = false
        do {
            let result = try self.manager.managedContext.fetch(request)
            return Observable.just(result)
        } catch {
            print("fetch request failed, managedobject")
            return Observable.empty()
        }
    }
    
    func deleteCity(_ city: City) {
        self.manager.managedContext.delete(city)
        self.manager.saveContext()
    }
}
